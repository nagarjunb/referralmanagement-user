import { NgModule,ErrorHandler , CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LocationStrategy, HashLocationStrategy} from '@angular/common';
import { MatModule } from './shared/mat.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { UserIdleModule } from 'angular-user-idle';
import { AppService } from '@shared/app.service';
import { SharedModule } from '@shared/index';
import { NeedAuthGuard } from '@shared/authGuard.service';
import { AppInsightService } from '@shared/app-insight';
import { CustomErrorHandler } from '@shared/custom-error-handler';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthenticationService } from './auth-service';
import { ReferralsModule } from './referrals/referrals.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarComponent } from './referrals/sidebar/sidebar.component';
import { HeaderComponent } from './shared/components/header/header.component';


@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    ReferralsModule,
    DashboardModule,
    HttpClientModule,
    NgxSpinnerModule,
    UserIdleModule.forRoot({idle: 1680, timeout: 1800, ping: 120}),
    MatModule
//    NgxSpinnerModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [AppInsightService, AppService, AuthenticationService,
    NeedAuthGuard, {provide: LocationStrategy, useClass: HashLocationStrategy},
    {
      provide: ErrorHandler,
      useClass: CustomErrorHandler,
       },],
  bootstrap: [AppComponent]
})
export class AppModule { }
