import { Injectable } from '@angular/core';
import { environment } from '@env/environment'
import { Constants } from 'src/constants';
import { AppService } from '@shared/app.service';

@Injectable()
export class AuthenticationService {

  constructor(private appService: AppService) { }

  medetails() {
    return this.appService.
      getAction(environment.apiUrl + Constants.endpointUrls.me);
  }

  versiondetails() {
    return this.appService.
      getAction(environment.apiUrl + Constants.endpointUrls.version);
  }

}
