import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferralsComponent } from './referrals.component';
import { ReferralsService } from './referrals.service';
import { ReferralsDetailsComponent } from './referrals-details/referrals-details.component';
import { ReferralsListComponent } from './referrals-list/referrals-list.component';
import { ReferralsMetricsComponent } from './referrals-metrics/referrals-metrics.component';

@NgModule({
  declarations: [ReferralsComponent, ReferralsDetailsComponent, ReferralsListComponent, ReferralsMetricsComponent],
  imports: [
    CommonModule,
  ],
   providers: [ReferralsService],
})
export class ReferralsModule { }
