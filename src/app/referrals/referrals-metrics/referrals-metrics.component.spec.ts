import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferralsMetricsComponent } from './referrals-metrics.component';

describe('ReferralsMetricsComponent', () => {
  let component: ReferralsMetricsComponent;
  let fixture: ComponentFixture<ReferralsMetricsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferralsMetricsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferralsMetricsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
