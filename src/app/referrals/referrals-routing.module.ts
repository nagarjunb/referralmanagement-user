import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ReferralsComponent} from './referrals.component'

const routes: Routes = [
  { path: 'referrals', component: ReferralsComponent },
  { path: 'referrals/details', component: ReferralsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ReferralsRoutingModule { }
