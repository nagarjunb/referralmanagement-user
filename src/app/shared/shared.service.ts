import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError } from 'rxjs';
import { map, catchError, timeout } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { AlertDialogComponent, AlertTypes } from './components';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { UserIdleService } from 'angular-user-idle';
import { Constants } from 'src/constants';
import { environment } from '@env/environment';

const TOKEN = 'TOKEN';
@Injectable()
export class SharedService {
  private maxTimeout = 80000;
  constructor(
    private http: HttpClient,
    private userIdle: UserIdleService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  timeOut = false
  // Navigate into another page
  navigateTo = (path: string): void => {
    this.router.navigate([path]);
  };

  // Clear local Storage
  clearData = (): void => {
    localStorage.clear();
  };

  public setToken(token: string): void {
    localStorage.setItem(TOKEN, token);
  }

  /**
   * Is Logged In Method
   */
  public isLogged() {
    return localStorage.getItem(TOKEN) != null;
  }

  stop() {
    this.userIdle.stopTimer();
  }

  stopWatching() {
    this.userIdle.stopWatching();
  }

  startWatching() {
    this.timeOut = false;
    this.userIdle.startWatching();
  }

  restart() {
    this.timeOut = false;
    this.userIdle.resetTimer();
  }

  public redirect() {
    window.location.assign(
      environment.apiUrl + Constants.endpointUrls.singlesignon
    );
  }
  public logout() {
    localStorage.clear();
    window.location.href = Constants.endpointUrls.logout;
  }

  sessionLogout() {
    const dialogRef = this.dialog.open(AlertDialogComponent, {
      panelClass: 'alert-dialog',
      disableClose: true,
      data: {
        msg: Constants.commonMessage.sessionTimeout,
        type: AlertTypes.ALERTCONFIRM,
      },
    });
    dialogRef.keydownEvents().subscribe((e) => {
      if (e.keyCode === 27) {
        dialogRef.close();
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        this.stop();
        this.stopWatching();
        this.startWatching();
      }
    });
  }

  // common get method
  getAction(path: string) {
    this.showLoader();
    return this.http.get(path).pipe(
      timeout(this.maxTimeout),
      map((res) => {
        this.hideLoader();
        return this.extractData(res);
      }),
      catchError((err) => {
        this.hideLoader();
        return throwError(err);
      })
    );
  }

  // common post method
  postAction(path: string, body: any) {
    this.spinner.show();
    return this.http.post(path, body).pipe(
      timeout(this.maxTimeout),
      map((res) => {
        this.hideLoader();
        return this.extractData(res);
      }),
      catchError((err) => {
        this.hideLoader();
        return throwError(err);
      })
    );
  }

  errorHandl(error: { error: { message: string }; status: any; message: any }) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

  showLoader() {
    this.spinner.show();
  }

  hideLoader() {
    this.spinner.hide();
  }

  private extractData(res: any) {
    if (res && res.status === 200) {
      return res.json() || {};
    } else {
      return res || {};
    }
  }
}
