import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export enum AlertTypes {
  ALERT = "alert",
  SUCCESS = "success",
  CONFIRM = "confirm",
  INFO = "info",
  WARN = "warn",
  ALERTCONFIRM  = 'alertconfirm',
  FIELDALERTCONFIRM  = 'fieldalertconfirm',
  PROCESSALERTCONFIRM  = 'processalertconfirm',
}

@Component({
  selector: 'app-alert-dialog',
  templateUrl: './alert-dialog.component.html',
  styleUrls: ['./alert-dialog.component.scss']
})
export class AlertDialogComponent implements OnInit {

  public types: typeof AlertTypes = AlertTypes;

  constructor(
    public dialogRef: MatDialogRef<AlertDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  // On Click Close
  onClickClose() {
    this.dialogRef.close();
  }

  // On Click Okay
  onClickOk() {
    this.dialogRef.close(true);

  }

}
