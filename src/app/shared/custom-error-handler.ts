import { stringify } from '@angular/compiler/src/util';
import {ErrorHandler, Injectable, ReflectiveInjector} from '@angular/core';
import {AppInsightService} from './app-insight';
import { AppInsights } from 'applicationinsights-js';


@Injectable()
export class CustomErrorHandler implements ErrorHandler {
  private appInsightService: AppInsightService;

  constructor() {
    const injector = ReflectiveInjector.resolveAndCreate([
      AppInsightService
    ]);
    this.appInsightService = injector.get(AppInsightService);
  }

  handleError(error: { message: any; stack: any; }) {
    const userInfomations: any =  JSON.parse(localStorage.getItem('userinfo') || '{}')
    console.log(error)
    const stackTrace: any = {
      name: userInfomations.userName,
      type: error.message,
      status_description: error.stack || 'No status description provided',
      error_message: error || 'No error response was provided',
      email_address: userInfomations ? userInfomations.emailAddress : 'No user information provided',
      customer_number: userInfomations ? userInfomations.customerNumber : 'No user information provided'
    };
  //  this.appInsightService.logError(error);
    AppInsights.trackException(stackTrace.type, stackTrace.error_response, stackTrace);
  }
}
