import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router/';
import { SharedService } from './shared.service';

@Injectable()
export class NeedAuthGuard implements CanActivate {
  constructor(private sharedService: SharedService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
 //   const redirectUrl = route['_routerState']['url'];
    if (this.sharedService.isLogged()) {
      return true;
    }

    this.router.navigateByUrl(
      this.router.createUrlTree(
        ['/referrals'], {
          queryParams: {
         //   redirectUrl
          }
        }
      )
    );

    return false;
  }
}
