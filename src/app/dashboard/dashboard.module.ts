import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboarDetailsComponent } from './dashboar-details/dashboar-details.component';


@NgModule({
  declarations: [DashboardComponent, DashboarDetailsComponent],
  imports: [
    CommonModule,
  ]
})
export class DashboardModule { }
