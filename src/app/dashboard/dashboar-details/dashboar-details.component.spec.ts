import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboarDetailsComponent } from './dashboar-details.component';

describe('DashboarDetailsComponent', () => {
  let component: DashboarDetailsComponent;
  let fixture: ComponentFixture<DashboarDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboarDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboarDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
