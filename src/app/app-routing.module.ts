import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NeedAuthGuard } from '@shared/authGuard.service';
import { DashboarDetailsComponent } from './dashboard/dashboar-details/dashboar-details.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ReferralsDetailsComponent } from './referrals/referrals-details/referrals-details.component';
import { ReferralsComponent } from './referrals/referrals.component';


const routes: Routes = [
  { path: '', redirectTo: 'referrals', pathMatch: 'full' },
  { path: 'referrals', component: ReferralsComponent },
  { path: 'referralsDetails', component: ReferralsDetailsComponent , canActivate: [NeedAuthGuard] },
  {
    path: 'referrals',
    component: ReferralsComponent,
    children: [{ path: 'details', component: ReferralsDetailsComponent , canActivate: [NeedAuthGuard] }],
  },
  { path: 'dashboard', component: DashboardComponent , canActivate: [NeedAuthGuard] },
  { path: 'dashboardDetails', component: DashboarDetailsComponent , canActivate: [NeedAuthGuard] },
  { path: '**', redirectTo: 'referrals', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
