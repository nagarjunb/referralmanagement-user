import { Component } from '@angular/core';
import { AuthenticationService } from './auth-service';
import { Constants } from 'src/constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserIdleService } from 'angular-user-idle';
import {
  AlertDialogComponent,
  AlertTypes,
} from '@shared/components/index';
import { MatDialog } from '@angular/material/dialog';
import { SharedService } from '@shared/shared.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent {
  collapsedSideBar = false;
  title = 'referral-management';
  data = [
    {
      name: 'ssss',
      date: 'fdgfdgfdgfd',
      age: 'dsfds',
    },
    {
      name: 'ddd',
      date: 'fdgfdgfdgfd',
      age: 'dsfds',
    },

    {
      name: 'rrrr',
      date: 'fdgfdgfdgfd',
      age: 'dsfds',
    },
  ];

  constructor(
    private authService: AuthenticationService,
    private dialog: MatDialog,
    private spinner: NgxSpinnerService,
    private userIdle: UserIdleService,
    private sharedService : SharedService
  ) {}

  ngOnInit(): void {
     // Start watching for user inactivity.
     this.userIdle.startWatching();

     // Start watching when user idle is starting.
     this.userIdle.onTimerStart().subscribe(count => {
       if(!this.sharedService.timeOut){
        this.sharedService.timeOut = true;
        this.sharedService.sessionLogout();
       }
     });

     // Start watch when time is up.
     this.userIdle.onTimeout().subscribe(() => {
       this.sharedService.logout();
     });


     this.getUserDetails()

  }

  receiveCollapsed($event: boolean) {
    this.collapsedSideBar = $event;
}

  getUserDetails = (): void => {
    this.authService.medetails().subscribe(
      (response) => {
        if (response) {
          this.setLoginData(response);
          this.sharedService.setToken(response.token)
        }
      },
      (error) => {
        if (error.status === 403) {
          const dialogRef = this.dialog.open(AlertDialogComponent, {
            panelClass: 'alert-dialog',
            disableClose: true,
            data: {
              msg: Constants.messages[403],
              type: AlertTypes.ALERTCONFIRM,
            },
          });
          dialogRef.keydownEvents().subscribe((e) => {
            if (e.keyCode === 27) {
              dialogRef.close();
            }
          });
          dialogRef.afterClosed().subscribe((result) => {
            this.sharedService.redirect();
          });
        } else {
            this.sharedService.redirect();
        }
      }
    );
  };

  setLoginData = (data: { accountNumber: string; token: string }): void => {
    localStorage.setItem('customerNumber', data.accountNumber);
    localStorage.setItem('TOKEN', data.token);
  };
}
