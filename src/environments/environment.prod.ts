export const environment = {
  production: true,
     apiUrl: '#{ApiUrl}#',
     appInsights: {
    instrumentationKey:'#{ApplicationInsights--InstrumentationKey}#'
    }
};
